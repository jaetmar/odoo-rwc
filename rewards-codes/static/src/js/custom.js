odoo.define('custom-button.custom_button', function (require) {
    "use strict";
    var core = require('web.core');
    const { useListener } = require('web.custom_hooks');
    const Registries = require('point_of_sale.Registries');
    const PaymentScreen = require('point_of_sale.PaymentScreen');
    const ProductScreen = require('point_of_sale.ProductScreen');
    var rpc = require('web.rpc');
    var partner, apiKey, code;

    const CustomProductScreen = (ProductScreen) => class extends ProductScreen {
        constructor() {
            super(...arguments);
            console.log("OVERRIDED PRODUCT");
            useListener('finalize-validation', this._showAlert);
        }

        mounted() {
            super.mounted();
            console.log('POS has been rendered.');
            // Aquí puedes agregar tus acciones personalizadas
            getRwCData();
        }

        _showAlert() {
            alert('Pago completado con éxito!');
        }
    };
    Registries.Component.extend(ProductScreen, CustomProductScreen);

    const CustomPaymentScreen = (PaymentScreen) => class extends PaymentScreen {
        constructor() {
            super(...arguments);
            console.log("OVERRIDED PAYMENT");
            useListener('finalize-validation', this._showAlert);
        }
        _showAlert() {
            alert('Pago completado con éxito!');
        }
    };
    Registries.Component.extend(PaymentScreen, CustomPaymentScreen);

    function getRwCData() {
        rpc.query({
            model: 'rewardscodes.config',
            method: 'get_all',
        })
        .then(function (response, something) {
            var data = JSON.parse(response);
            console.log("DATA: ")
            console.log(data)
            partner = data['phone'][0];
            apiKey = data['api_key'][0];
            code = data['default_phone_code'][0];

            if ([partner, apiKey, code].includes(undefined) || [partner, apiKey, code].includes([])) {
                console.log("ERROR, could not get RwC data")
                $('head').append(`
                    <style>
                        .rwc-error-lbl {
                            color: red;
                            font-size: 23px;
                            text-align: center;
                            width: -webkit-fill-available;
                            padding: 7px;
                        }
                    </style>
                `);

                $('.control-buttons').append(`
                    <span class="rwc-error-lbl">Rewards Codes has not been configured!</span>
                `);
                return
            }

            console.log("DATA RETRIEVED, rendering RwC UI")
            renderRewardsCodesElements();
        });
    }

    function renderRewardsCodesElements() {
        addCss();
        $('.rwc-button').remove();
        $('#exampleModal').remove();
        
        $('.cash-move-button').after(`
            <div class="ticket-button rwc-button">
                <i aria-hidden="true" class="fa fa-gift"></i>
                <div style="margin-left: 5px;">Rewards Codes</div>
            </div>
        `)

        $('body').append(`
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" style="display: flex; justify-content: center;">
            <div class="modal-dialog" style="padding: 0; background-color: transparent;width: 350px;overflow: hidden;">
                <div class="modal-content" style="display: flex;flex-flow: column;height:-webkit-fill-available;">
                    <div class="modal-header" style="background-color: #72246c;border-radius: 3px;height: 38px;color: white;display: flex;justify-content: space-between;padding: 12px;">
                        <span class="modal-title" style="align-self: center;">RwC</span>
                        <i class="fa fa-close rwc-close rwc-btn" style="padding: 4px 6px;border-radius: 50%;align-self: center; font-size: 28px;"></i>
                    </div>
                    <div class="modal-body" style="padding: 12px;overflow-y: scroll;flex-grow: 2;background-color: white;flex-flow: column;">
                        <div id="rewardsView" style="display: flex;flex-flow: column;">
                            <span style="font-size: larger;font-weight: bold;">Número telefónico</span>
                            <div style="display: flex;">
                                <input style="margin: 5px;" type="text" value="+52" id="rewardsPhone">
                                <button class="rwc-btn" id="rewardsButton" type="button">Registrar número</button>
                            </div>
                        </div>

                        <div id="codesView" style="display: flex;flex-flow: column;">
                            <span style="font-size: larger;font-weight: bold;">Número telefónico</span>
                            <div style="display: flex;">
                                <input style="margin: 5px;" type="text" value="+52" id="codesPhone">
                                <button class="rwc-btn" id="codesConsultButton" type="button">Consultar</button>
                            </div>

                            <span style="margin: 5px 0px;font-size: larger;font-weight: bold;">Código</span>
                            <div style="display: flex;">
                                <input style="margin: 5px;" type="text" id="codesCode">
                                <button class="rwc-btn" id="codesSendButton" type="button">Enviar código</button>
                            </div>

                            <span style="margin: 5px 0px;font-size: larger;font-weight: bold;">Número de visitas</span>
                            <div style="display: flex;">
                                <input style="margin: 5px;" type="text" id="codesVisits">
                                <button class="rwc-btn" id="codesRedeemButton" type="button">Canjear</button>
                            </div>
                        </div>

                        <div class="rwc-message"></div>
                    </div>
                <div class="modal-footer" style="display: flex;background-color: #72246c;color:white;">
                    <div class="rwc-btn" id="btnRewardsView" style="margin: 0;display:flex;justify-content:center;flex-grow: 2;align-self: center;font-size: x-large;">Rewards</div>
                    <div class="rwc-btn" id="btnCodesView" style="margin: 0;display:flex;justify-content:center;flex-grow: 2;align-self: center;font-size: x-large;">Codes</div>
                </div>
            </div>
        </div>
        `);
        $('#exampleModal').hide();

        $('.control-buttons').append(`
            <div class="button rewards rwc-button" style="background-color: #72246c; color: white;">
                <i class="fa fa-gift"></i>&nbsp; Reward
            </div>
        `);

        $('.rwc-button').on('click', e => $('#exampleModal').show());
        $('.rwc-close').on('click', e => $('#exampleModal').hide());

        $('#rewardsButton').on('click', e => reward(partner, $('#rewardsPhone').val(), apiKey, 1));
        $('#codesConsultButton').on('click', e => consult(partner, $('#codesPhone').val(), apiKey));
        $('#codesSendButton').on('click', e => sendCode($('#codesPhone').val()));
        $('#codesRedeemButton').on('click', e => redeem(partner, $('#codesPhone').val(), $('#codesCode').val(), apiKey, parseInt($('#codesVisits').val())));
        
        $('#btnRewardsView').on('click', e => {
            $('#rewardsView').show();
            $('#codesView').hide();
        });
        
        $('#btnCodesView').on('click', e => {
            $('#rewardsView').hide();
            $('#codesView').show();
        });

        $('#rewardsView').show();
        $('#codesView').hide();
        getRewardsLeft(partner, apiKey);        
    }


    function consult(partner, customer, apiKey) {
        if(customer.length < 7) {
            message('El número no es válido', 'red');
            return;
        }

        showLoader();

        var url = 'https://apig.systems:8000/rwc/get_phone_rewards_by_partner?id=' + partner.replace('+', '%2B');
        var body = { 'phone_id': customer }

        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(body),
            headers: {
                'rwc-id': apiKey,
                'Content-Type':'application/json'
            },
            success: function(data){
                hideLoader();
                console.log(data)
                if(data.status === 'error') message(data.message, 'red');
                else {
                    var text = `El teléfono ${customer} tiene ${data.data.points} en ${data.data.name}</br></br>`;

                    if(data.data.prizes.length > 0) {
                        text += `<span style="font-weight: bold;">Promociones desbloqueadas:</span> </br>`;
                        data.data.prizes.forEach(e => {
                            text += `${e.name} (${e.quantity} visitas): ${e.prize} </br>`;
                        });
                        text += `</br>`;
                    }

                    if(data.data.prizes.length > 0) {
                        text += `<span style="font-weight: bold;">Historial:</span> </br>`;
                        data.history.forEach(e => {
                            text += `${e.type} (${e.quantity} visitas): ${e.date} </br>`;
                        });
                    }
                    message(text, 'black', "font-size: 18px;text-align: center;");
                }
            }
        });
    }

    function sendCode(customer) {
        if(customer.length < 7) {
            message('El número no es válido', 'red');
            return;
        }

        showLoader();

        var url = 'https://apig.systems:8000/rwc/get_phone_code_secure?id=' + customer.replace('+', '%2B');

        $.ajax({
            type: "GET",
            url: url,
            headers: { 'rwc-id': apiKey, 'Content-Type':'application/json' },
            success: function(data){
                hideLoader();
                console.log(data)
                if(data.status === 'error') message(data.message, 'red');
                else message(`Code sent`, 'green');
            }
        });
    }

    function getRewardsLeft(partner, apiKey) {
        var url = 'https://apig.systems:8000/rwc/get_rewards_left?id=' + partner.replace('+', '%2B');

        $.ajax({
            type: "GET",
            url: url,
            headers: {
                'rwc-id': apiKey,
                'Content-Type':'application/json'
            },
            success: function(data){
                console.log(data)
                if(data.status === 'error') message(data.message, 'red');
                else $('.modal-title').html(`${data.rewards} RwC`);
            }
        });
    }

    function reward(partner, customer, apiKey, rwc) {

        if(customer.length < 7) {
            message('El número no es válido', 'red');
            return;
        }

        showLoader();

        var url = 'https://apig.systems:8000/rwc/add_reward?id=' + partner.replace('+', '%2B');
        var body = {
            'phone_id': customer,
            'quantity': rwc
        }

        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(body),
            headers: {
                'rwc-id': apiKey,
                'Content-Type':'application/json'
            },
            success: function(data){
                hideLoader();
                console.log(data)
                if(data.status === 'error') message(data.message, 'red');
                else {
                    message('Número registrado', 'green');
                    $('#rewardsPhone').val(code);
                    getRewardsLeft(partner, apiKey);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                hideLoader();
                message(`ERROR: ${XMLHttpRequest} ÑÑ ${errorThrown} ÑÑ ${textStatus}`, 'red');
            }
        });
    }

    function redeem(partner, customer, phoneCode, apiKey, rwc) {

        if(customer.length < 7) {
            message('El número no es válido', 'red');
            return;
        }

        showLoader();

        var url = 'https://apig.systems:8000/rwc/add_redeem?id=' + partner.replace('+', '%2B');
        var body = {
            'body': { reference: '' },
            'phone_id': customer,
            'code': phoneCode,
            'quantity': rwc
        }

        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(body),
            headers: {
                'rwc-id': apiKey,
                'Content-Type':'application/json'
            },
            success: function(data){
                hideLoader();
                console.log(data)
                if(data.status === 'error') message(data.message, 'red');
                else {
                    message('Canjeo exitoso', 'green');
                    $('#codesPhone').val(code);
                    $('#codesCode').val('');
                    $('#codesVisits').val('');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                hideLoader();
                message(`ERROR: ${XMLHttpRequest} ÑÑ ${errorThrown} ÑÑ ${textStatus}`, 'red');
            }
        });
    }

    function message(message, color, params="text-align: center; font-size: 25px;") {
        $('.rwc-message').html(`
            <div class="rwc-message" style="color: ${color};${params}">
                ${message}
            </div>
        `);
    }

    function showLoader() {
        $('body').append(`  
            <div class="rwc-loader-container"><div class="rwc-loader"></div></div>
        `);
    }

    function hideLoader() {
        $('.rwc-loader-container').remove();
    }

    function addCss() {
        console.log("ADD CSS")
        $('head').append(`
        <style>
            .rewards {
                background-color: #72246c;
                color: white;
                flex-grow: 1;
                border: solid 1px #bfbfbf;
                display: inline-block;
                line-height: 38px;
                min-width: 80px;
                text-align: center;
                border-radius: 3px;
                padding: 0px 10px;
                font-size: 18px;
                margin-left: 6px;
                margin-bottom: 6px;
                cursor: pointer;
                overflow: hidden;
                transition: all linear 150ms;
            }

            .rwc-btn {
                background-color: #72246c;
                border: none;
                color: white;
                padding: 16px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                transition-duration: 0.4s;
                cursor: pointer;
              }
              
            .rwc-btn:hover {
                background-color: #757575;
                color: white;
            }
              
            .rwc-loader-container {
                position: absolute;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                background-color: #00000085;
            }

            .rwc-loader {
                position: absolute;
                margin: auto;
                left: 0;
                right: 0;
                top: 0;
                bottom: 0;
                border: 4px solid#f3f3f3;
                border-radius: 50%;
                border-top: 4px solid#72246c;
                width: 80px;
                height: 80px;
                -webkit-animation: rwc-spin 2s linear infinite; /* Safari */
                animation: rwc-spin 2s linear infinite;
            }

            /* Safari */
            @-webkit-keyframes rwc-spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes rwc-spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
        </style>
        `);
    }
});



