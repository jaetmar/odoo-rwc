# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
import json

class RewardsCodesConfig(models.Model):

    _name = 'rewardscodes.config'
    _description = 'Rewards Codes Config'

    phone = fields.Char('Owner Phone', required=True, help='This phone will be associated to your company')
    default_phone_code = fields.Char("Default phone's code", required=True, help='This is default phone code. Ej. +52 or +1')
    # reward_pct = fields.Integer('Rewards percentage', help='Percentage of the total sale which will be rewarded to the customer')
    api_key = fields.Char('API Key', required=True, help='This developer key allows the connection to Rewards Codes')

    @api.model
    def get_all(self):
        rwc_config = self.env['rewardscodes.config'].search([])
        data = {
            'phone': rwc_config.mapped('phone'),
            'default_phone_code': rwc_config.mapped('default_phone_code'),
            # 'reward_pct': rwc_config.mapped('reward_pct'),
            'api_key': rwc_config.mapped('api_key'),
        }
        return json.dumps(data)